﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProcessMonitor.Configurations;
using ProcessMonitor.Core;
using ProcessMonitor.Models;

namespace ProcessMonitor.Controllers
{
    public class CallbacksController : Controller
    {
        private readonly ILogger<CallbacksController> _logger;
        private readonly IMonitorService _monitorService;
        private readonly ICallbacksStorageService _callbackStorageService;
        private readonly AppSettings _appSettings;
        public CallbacksController(IOptions<AppSettings> settings, IMonitorService monitorService, ICallbacksStorageService callbackStorageService, ILogger<CallbacksController> logger)
        {
            _logger = logger;
            _monitorService = monitorService;
            _appSettings = settings.Value;
            _callbackStorageService = callbackStorageService;
            _monitorService.CallbackCall += CallbackCalls;
        }
        private async Task CallbackCalls(MessageModel message)
        {
            try
            {
                var callbacks = await _callbackStorageService.GetCallbacksAsync();
                var tasks = new List<Task>();
                callbacks.ToList().ForEach(callback => tasks.Add(callback.Call(message)));
                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                _logger.LogError(1, ex, "We should send message, but any exception throwed!");
            }
        }
        [HttpGet("callbacks")]
        public async Task<IList<CallbackModel>> ViewCallbacks() => await _callbackStorageService.GetCallbacksAsync();
        [HttpGet("callbacks/{id}")]
        public async Task<CallbackModel> ViewCallback(Guid id) => await _callbackStorageService.GetCallbackAsync(id);
        [HttpPost("callbacks/register")]
        public async Task RegisterCallback([FromBody]CallbackModel callback) => await _callbackStorageService.RegisterCallbackAsync(callback);
        [HttpGet("callbacks/{id}/delete")]
        public async Task DeleteCallback(Guid id) => await _callbackStorageService.RemoveCallbackAsync(id);
    }
}
