﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProcessMonitor.Configurations;
using ProcessMonitor.Core;
using ProcessMonitor.Models;

namespace ProcessMonitor.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index() => View();
    }
}
