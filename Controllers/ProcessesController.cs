﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProcessMonitor.Configurations;
using ProcessMonitor.Core;
using ProcessMonitor.Models;

namespace ProcessMonitor.Controllers
{
    public class ProcessesController : Controller
    {
        private readonly ILogger<ProcessesController> _logger;
        private readonly IMonitorService _monitorService;
        private readonly AppSettings _appSettings;
        private static MessageModel _lastMessage;
        public ProcessesController(IOptions<AppSettings> settings, IMonitorService monitorService, ILogger<ProcessesController> logger)
        {
            _logger = logger;
            _monitorService = monitorService;
            _appSettings = settings.Value;
            _monitorService.CallbackCall += CallbackCallHandler;
        }
        public IActionResult Index() => View(BuildFullModel());
        [HttpGet("processes/api")]
        public IActionResult ApiIndex() => Json(BuildFullModel());
        [HttpGet("processes/api/polling")]
        public async Task<IActionResult> DoLongPolling()
        {
            var message = await WaitAlarm();
            if (message == null) return new ObjectResult(new { Status = "Long polling timeout!" });
            else return new ObjectResult(new { Status = "OK", Data = message });
        }
        private Task CallbackCallHandler(MessageModel message) => Task.Run(() => _lastMessage = message);
        private async Task<MessageModel> WaitAlarm()
        {
            _lastMessage = null;
            var start = DateTime.UtcNow;
            while ((DateTime.UtcNow - start).TotalMilliseconds < _appSettings.PollingTimeout)
            {
                if (_lastMessage != null) return _lastMessage;
                await Task.Delay(100);
            }
            if (_lastMessage != null) return _lastMessage;
            return null;
        }
        private FullModel BuildFullModel() => new FullModel() { State = _monitorService.GetState(), Processes = _monitorService.GetProcesses() };
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
