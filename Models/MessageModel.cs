using System.Text;
using System.Text.Json.Serialization;

namespace ProcessMonitor.Models
{
    public class MessageModel
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }
        public MessageModel() { }
        public MessageModel(StateModel state, double ramTreshold, double cpuTreshold)
        {
            var builder = new StringBuilder();
            if (state.RAMusage > ramTreshold) builder.Append($"ALARM! Current RAM usage is {state.RAMusage}%, when RAM treshold is {ramTreshold}%!");
            if (state.CPUusage > cpuTreshold) builder.Append($"{(builder.Length > 0 ? "\n" : "")}ALARM! Current CPU usage is {state.CPUusage}%, when CPU treshold is {cpuTreshold}%!");
            this.Message = builder.ToString();
        }
    }
}
