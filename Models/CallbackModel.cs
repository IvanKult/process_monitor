using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ProcessMonitor.Models
{
    public class CallbackModel
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }
        [JsonPropertyName("url")]
        public string Url { get; set; }
        public async Task Call(MessageModel message)
        {
            var request = (HttpWebRequest)WebRequest.Create(this.Url);
            var data = Encoding.ASCII.GetBytes(JsonSerializer.Serialize(message));
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                await stream.WriteAsync(data, 0, data.Length);
            }
            var response = (HttpWebResponse)await request.GetResponseAsync();
        }
    }
}
