using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ProcessMonitor.Models
{
    public class FullModel
    {
        [JsonPropertyName("processes")]
        public IList<ProcessModel> Processes { get; set; }
        [JsonPropertyName("state")]
        public StateModel State { get; set; }
    }
}
