using System.Text.Json.Serialization;

namespace ProcessMonitor.Models
{
    public class StateModel
    {
        [JsonPropertyName("cpu-usage")]
        public double CPUusage { get; set; }
        [JsonPropertyName("ram-usage")]
        public double RAMusage { get; set; }
    }
}
