using System.Diagnostics;
using System.Text.Json.Serialization;

namespace ProcessMonitor.Models
{
    public class ProcessModel
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("working-set-64")]
        public long WorkingSet64 { get; set; }
        [JsonPropertyName("private-memory-size-64")]
        public long PrivateMemorySize64 { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        public ProcessModel() { }
        public ProcessModel(Process process)
        {
            Id = process.Id;
            Name = process.ProcessName;
            WorkingSet64 = process.WorkingSet64;
            PrivateMemorySize64 = process.PrivateMemorySize64;
        }
    }
}
