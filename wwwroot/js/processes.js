$(document).ready(function () { process(); longPolling(); });

function process() {
    try {
        refresh();
    } catch (err) {
        console.log(err);
    }
    setTimeout(process, 3000);
}

function refresh() {
    $.ajax({ url: "processes/api" }).done(function (data) {
        viewState(data.state);
        viewProcesses(data.processes);
    });
}

function viewState(state) {
    $('#cpu').html(`${state['cpu-usage']} %`);
    $('#ram').html(`${state['ram-usage']} %`);
}

function viewProcesses(processes) {
    builder = '<tr><th>Id</th><th>Name</th><th>PrivateMemorySize64</th><th>WorkingSet64</th></tr>';
    processes.forEach(function (value) {
        builder += `<tr><td>${value.id}</td><td>${value.name}</td><td>${value['private-memory-size-64']}</td><td>${value['working-set-64']}</td></tr>`;
    });
    $('#processes-table>tbody').html(builder);
}

function longPolling() {
    try {
        $.ajax({
            url: "processes/api/polling", success: function (data) {
                if (data.status == 'OK') {
                    $('.notification').html(data.data.message);
                    $('.notification').css('opacity', 1);
                    $('.notification').animate({ opacity: 0 }, 3000, function () { longPolling(); });
                }
                else
                    longPolling();
            }, dataType: "json", timeout: 30000
        });
    } catch (err) {
        console.log(err);
        setTimeout(longPolling(), 1000);
        longPolling();
    }
}

function viewAlarmMessage(message) {
}