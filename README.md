# Process monitor

There is web tool for monitoring VM processes.

## Web pages

There are only two web pages
[http://localhost:5000/](http://localhost:5000/) and [http://localhost:5000/processes](http://localhost:5000/processes)

On the **Home page** we can see instruction and description. On the **Processes page**  we can see current system state and all runned processed.

We will see notifiction, when CPU or RAM will greater than configured tresholds.

## API endpoints

This web app provide follow api endpoints:

[http://localhost:5000/processes/api](http://localhost:5000/processes/api)         - returns state and processes information

[http://localhost:5000/processes/api/polling](http://localhost:5000/processes/api/polling) - implement long polling mechanism for get notifications

[http://localhost:5000/callbacks/](http://localhost:5000/callbacks/)            - returns all registered callbacks

[http://localhost:5000/callbacks/id](http://localhost:5000/callbacks/id)          - returns registered callback by id

[http://localhost:5000/callbacks/register](http://localhost:5000/callbacks/register)    - register new callback

[http://localhost:5000/callbacks/{id}/delete](http://localhost:5000/callbacks/{id}/delete) - delete registered callbacks by id

### Callbacks mechanism

Client can register callback endpoint (http url), and app will send notifications to this endpoint via Http  (GET).

In current version all registered callback saved in json file.

## Architecture

There is classic ASP.NET Core MVC web application.

``` empty
├── appsettings.json    //application configuration
├── Configurations      //configuration models
│   ├── AppSettings.cs  
│   ├── CallbacksStorageConfiguration.cs
│   └── MonitorConfiguration.cs
├── Controllers         //MVC controllers
│   ├── CallbacksController.cs  //Callbacks mechanism
│   ├── HomeController.cs       //Home page
│   └── ProcessesController.cs  //Processes (main) controller
├── core
│   ├── CallbacksStorageService.cs  // Simple implementation for ICallbacksStorageService. All callbacks will saved in file in json format
│   ├── ICallbacksStorageService.cs // Interface for CallbackStorages. Provide method for store callback endpoints.
│   ├── IMonitorService.cs  //Default IMonitorService implementation
│   ├── MonitorService.cs   //Interface for MonitorServices. Provide methods for get current VM state and processes. Call "CallbackCall" event, when any alarm happend
│   └── ShellHelper.cs      //Class for call system shell
├── Models
│   ├── CallbackModel.cs
│   ├── ErrorViewModel.cs
│   ├── FullModel.cs
│   ├── MessageModel.cs
│   ├── ProcessModel.cs
│   └── StateModel.cs
├── process_monitor.csproj
├── Program.cs
├── Properties
│   └── launchSettings.json
├── README.md
├── Startup.cs
├── Views
│   ├── Home
│   │   └── Index.cshtml
│   ├── Processes
│   │   └── Index.cshtml
│   ├── Shared
│   │   ├── Error.cshtml
│   │   ├── _Layout.cshtml
│   │   └── _ValidationScriptsPartial.cshtml
│   ├── _ViewImports.cshtml
│   └── _ViewStart.cshtml
└── wwwroot
    ├── css
    │   ├── processes.css   //Processes page styles
    │   └── site.css
    ├── favicon.ico
    └── js
        ├── processes.js    //Processes page script
        └── site.js

```

## What could do in future

- save registered callbacks to db (e.g. Sqlite or Mongo). It could realized if develop new implementation for ICallbacksStorageService.
- use Vue.js or React for FrontEnd instead of jQuery (for more extendable logic)
- beautify pages for more pretty view
- implement access control
- use websockets instead of longPolling
- extend ProcessModel for provide mor information about processes
- extend StateModel for provide more information about VM state
- implement sort in processes table (e.g. by id, name, CPU or RAM usage)
