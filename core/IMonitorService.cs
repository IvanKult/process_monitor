using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProcessMonitor.Models;

namespace ProcessMonitor.Core
{
    /// <summary>
    /// Alarm notification handler delegate
    /// </summary>
    public delegate Task CallbackHandler(MessageModel message);
    /// <summary>
    /// Interface for MonitorServices. Provide methods for get current VM state and processes. Call "CallbackCall" event, when any alarm happend
    /// </summary>
    public interface IMonitorService : IDisposable
    {
        event CallbackHandler CallbackCall;
        IList<ProcessModel> GetProcesses();
        StateModel GetState();
        void Refresh();
        void Start();
        void Stop();
    }
}
