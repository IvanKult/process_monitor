using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using ProcessMonitor.Configurations;
using ProcessMonitor.Models;

namespace ProcessMonitor.Core
{
    /// <summary>
    /// Default IMonitorService implementation
    /// </summary>
    public class MonitorService : IMonitorService
    {
        private static ConcurrentDictionary<int, ProcessModel> _processes;
        private static StateModel _state;
        private static CancellationTokenSource _cts;
        private static Thread _mainThread;
        private static MonitorConfiguration _configuration;
        public event CallbackHandler CallbackCall;
        public MonitorService(MonitorConfiguration configuration) => _configuration = configuration;

        public void Start()
        {
            _cts = new CancellationTokenSource();
            _processes = new ConcurrentDictionary<int, ProcessModel>();
            _mainThread = new Thread(Main);
            _mainThread.Start();
        }
        public void Stop()
        {
            _cts.Cancel();
            _mainThread.Join();
        }
        private void Main()
        {
            while (!_cts.IsCancellationRequested)
            {
                this.Refresh();
                Task.Delay(_configuration.DelayTime).Wait();
            }
        }
        public StateModel GetState() => _state;
        public IList<ProcessModel> GetProcesses()
        {
            var result = new List<ProcessModel>();
            foreach (var key in _processes.Keys)
            {
                if (_processes.TryGetValue(key, out ProcessModel value)) result.Add(value);
            }
            return result;
        }
        public void Refresh()
        {
            var processes = Process.GetProcesses().ToList();
            var processesIds = processes.Select(process => process.Id).ToList();
            foreach (var key in _processes.Keys)
            {
                if (!processesIds.Contains(key)) _processes.TryRemove(key, out ProcessModel value);
            }
            processes.ForEach(process => _processes.AddOrUpdate(process.Id, new ProcessModel(process), (id, oldVal) => new ProcessModel(process)));
            _state = LoadState();
            if (!CheckState(_state) && CallbackCall != null) CallbackCall(new MessageModel(_state, _configuration.RamTreshold, _configuration.CpuTreshold));
        }
        /// <summary>
        /// Get VM state. Used system tools for get info (Bash commands for Linux and cmd commands for Windows)
        /// </summary>
        private StateModel LoadState()
        {
            try
            {
                double ram = 0, cpu = 0;
                var isOk = false;
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    //Untested, but should works
                    var findInString = new Func<string, double>(source => double.Parse(source.Split(new char[] { ':' })[1].Replace(" ", "").Replace("MB", "").Replace(',', '.')));
                    var totalRamStr = ShellHelper.WinCmd("systeminfo | findstr /C:\"Total Physical Memory\"");
                    var totalRam = findInString(totalRamStr);
                    var availRamStr = ShellHelper.WinCmd("systeminfo | findstr /C:\"Available Physical Memory\"");
                    var availRam = findInString(availRamStr);
                    ram = (totalRam - availRam) / totalRam * 100d;
                    var cpuStr = ShellHelper.WinCmd("Powershell \"[string][int](Get-Counter '\\Processor(*)\\% Processor Time').Countersamples[0].CookedValue + '%'\"");
                    cpuStr = cpuStr.Split('\n')[0].Replace("%", "").Replace(',', '.');
                    cpu = double.Parse(cpuStr);
                    isOk = true;
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    var ramStr = Core.ShellHelper.LinuxBash("free free -b | awk 'NR==2{printf \"%.2f\", $3*100/$2 }'");
                    ram = double.Parse(ramStr, CultureInfo.InvariantCulture);
                    var cpuStr = Core.ShellHelper.LinuxBash("top -bn 1 -o %CPU |tail +8 | awk '{print($9)}' |sed -s 's/,/./g' | head | tr '\n' '+' | sed -E 's/\\+$/\\n/g'|bc ").Replace("\n", "");
                    cpu = double.Parse(cpuStr, CultureInfo.InvariantCulture);
                    isOk = true;
                }
                if (!isOk) return null;
                return new StateModel()
                {
                    RAMusage = ram,
                    CPUusage = cpu
                };
            }
            catch { return null; }
        }
        private bool CheckState(StateModel state) => (state.CPUusage < _configuration.CpuTreshold && state.RAMusage < _configuration.RamTreshold);
        public void Dispose() => this.Stop();
    }
}
