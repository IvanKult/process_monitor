using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProcessMonitor.Models;

namespace ProcessMonitor.Core
{
    /// <summary>
    /// Interface for CallbackStorages. Provide method for store callback endpoints.
    /// </summary>
    public interface ICallbacksStorageService
    {
        Task RegisterCallbackAsync(CallbackModel callback);
        Task RemoveCallbackAsync(Guid id);
        Task<CallbackModel> GetCallbackAsync(Guid id);
        Task<IList<CallbackModel>> GetCallbacksAsync();
    }
}
