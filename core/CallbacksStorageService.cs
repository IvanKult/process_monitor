using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using ProcessMonitor.Configurations;
using ProcessMonitor.Models;

namespace ProcessMonitor.Core
{
    /// <summary>
    /// Simple implementation for ICallbacksStorageService. All callbacks will saved in file in json format
    /// </summary>
    public class CallbacksStorageService : ICallbacksStorageService
    {
        private static CallbacksStorageConfiguration _configuration;
        public CallbacksStorageService(CallbacksStorageConfiguration configuration) => _configuration = configuration;
        public async Task<IList<CallbackModel>> GetCallbacksAsync()
        {
            var json = "[]";
            if (File.Exists(_configuration.FilePath))
            {
                using (var reader = new StreamReader(_configuration.FilePath))
                {
                    json = await reader.ReadToEndAsync();
                }
            }
            return JsonSerializer.Deserialize<List<CallbackModel>>(json);
        }
        public async Task RegisterCallbackAsync(CallbackModel callback)
        {
            var callbacks = await GetCallbacksAsync();
            if (callbacks.Where(x => x.Id == callback.Id).Count() > 0) throw new ArgumentException("Callback with this id exists yet!");
            callbacks.Add(callback);
            await SaveAsync(callbacks);
        }
        public async Task RemoveCallbackAsync(Guid id)
        {
            var callbacks = await GetCallbacksAsync();
            var filtered = callbacks.Where(x => x.Id == id);
            if (filtered.Count() == 0) throw new ArgumentException("Callback with this id doesn't exists!");
            callbacks.Remove(filtered.First());
            await SaveAsync(callbacks);
        }
        public async Task<CallbackModel> GetCallbackAsync(Guid id)
        {
            var callbacks = await GetCallbacksAsync();
            var filtered = callbacks.Where(x => x.Id == id);
            if (filtered.Count() == 0) throw new ArgumentException("Callback with this id doesn't exists!");
            return filtered.First();
        }
        private async Task SaveAsync(IList<CallbackModel> callbacks)
        {
            using (var writer = new StreamWriter(_configuration.FilePath, false))
            {
                await writer.WriteAsync(JsonSerializer.Serialize(callbacks));
            }
        }
    }
}
