
namespace ProcessMonitor.Configurations
{
    /// <summary>
    /// Class, which contains all application configurations and saved in appsettings.json in "AppSettings" Section
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// LongPolling timeout for wait alarm message in ProcessesController
        /// </summary>
        public int PollingTimeout { get; set; }
        /// <summary>
        /// Settings for MonitorService
        /// </summary>
        public MonitorConfiguration Monitor { get; set; }
        /// <summary>
        /// Settings for CallbacksStorageService
        /// </summary>
        public CallbacksStorageConfiguration CallbacksStorage { get; set; }
    }
}