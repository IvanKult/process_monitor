
namespace ProcessMonitor.Configurations
{
    /// <summary>
    /// Settings for CallbacksStorageService
    /// </summary>
    public class CallbacksStorageConfiguration
    {
        /// <summary>
        /// Path to file, where callbacks will saved
        /// </summary>
        public string FilePath { get; set; }
    }
}