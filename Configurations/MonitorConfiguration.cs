
namespace ProcessMonitor.Configurations
{
    /// <summary>
    /// Settings for MonitorService
    /// </summary>
    public class MonitorConfiguration
    {
        /// <summary>
        /// MonitorService should notify, when RAM usage will greater than this value
        /// </summary>
        public double RamTreshold { get; set; }
        /// <summary>
        /// MonitorService should notify, when CPU usage will greater than this value
        /// </summary>
        public double CpuTreshold { get; set; }
        /// <summary>
        /// Refresh period for MonitorService
        /// </summary>
        public int DelayTime { get; set; }
    }
}